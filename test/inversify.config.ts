import "reflect-metadata";
import { Container } from "inversify";
import { IRepository } from "../src/types/repository";
import REPOSITORY_TYPES from "../src/types/serviceTypes";
import DefaultRepository from "./mockRepositories/DefaultRepository";

const container = new Container();

// Create a mock for all repos that don't have explicit mocks
Object.values(REPOSITORY_TYPES).map((st) => {
  if (!container.isBound(st)) {
    container.bind<IRepository<any>>(st).to(DefaultRepository);
  }
});

export default container;
