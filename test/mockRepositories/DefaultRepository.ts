import { IRepository } from "../../src/types/repository";
import { injectable } from "inversify";

@injectable()
class DefaultRepository implements IRepository<any> {
  create = async (item: Partial<any>): Promise<any> => {
    return {
      id: 1,
      ...item,
    };
  };

  delete = async (_id: number): Promise<void> => {
    return;
  };

  get = async (id: number, _eagerLoad: object | null): Promise<any> => {
    return {
      id,
    };
  };

  getAll = async (_eagerLoad: object | null): Promise<any[]> => {
    return [];
  };

  getWhere = async (
    _params: Partial<any>,
    _eagerLoad: object | null
  ): Promise<any[]> => {
    return [];
  };

  update = async (id: number, item: Partial<any>): Promise<any> => {
    return {
      id,
      ...item,
    };
  };
}

export default DefaultRepository;
