export interface IRepository<T> {
  get: (id: number, eagerLoad: object | null) => Promise<T>;
  getWhere: (params: Partial<T>, eagerLoad: object | null) => Promise<T[]>;
  getAll: (eagerLoad: object | null) => Promise<T[]>;
  create: (item: Partial<T>) => Promise<T>;
  update: (id: number, item: Partial<T>) => Promise<T>;
  delete: (id: number) => Promise<void>;
}
