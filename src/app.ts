import "reflect-metadata";
import express from "express";
import { errors } from "celebrate";
import logger from "morgan";
//@ts-ignore
import container from "./inversify.config";
const app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(errors());

export default app;
